const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

// Get  all  tasks
router.get("/", (request, response) => {
	TaskController.getAll().then(result => response.send(result))
})

// Create new task
router.post("/", (request, response) => {
	TaskController.createTask(request.body).then(result => response.send(result))
})


router.put("/:id", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then(result => {
		response.send(result)
	})
})

router.delete("/:id", (request, response) => {
	TaskController.deleteTask(request.params.id, request.body).then(result => response.send(result))
})

module.exports =  router