// Postman steps: 
// get ID from "get all tasks"
// in update task (PUT), add the ID and paste it in the url
// in the same task, go to body, and input a raw JSON (see below)
/*
{
	"name": "Setup painting materials"
}
*/

// in delete task, use the same ID


// Server Dependencies
const express = require("express")
const mongoose = require("mongoose")
require("dotenv").config() // .env initialization
const taskRoutes = require("./routes/taskRoutes.js")

// MongoDB Connection
mongoose.connect(`mongodb+srv://nillyp:${process.env.MONGODB_PASSWORD}@zuittm0.qy65ozq.mongodb.net/todo-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection 

// Listeners
db.on("error", () => console.log("Something went wrong."))
db.once("open", () => console.log("Connected to MongoDB!"))


// Server Setup
const app = express()
const port = 3001

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))


// Routes
app.use("/tasks", taskRoutes) //  first arg is endpoint, second is the route



// Server Listening
app.listen(port, () => console.log(`Server running at localhost:${port}`))

